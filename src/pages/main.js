import React, { Component } from 'react';

import Header from '../components/Header/Header';
import Hero from '../components/Hero/Hero';
import ColabComp from '../components/Colab/ColabComp';
import ReleasesComp from '../components/Releases/ReleasesComp';
import MerchComp from '../components/Explore/MerchComp';
import SoundPacks from "../components/Explore/SoundPacksComponent";
import Footer from '../components/Footer/Footer';
import ModalSearch from '../components/Modal/ModalSearch';
import ModalMenu from '../components/Modal/ModalMenu';
import Scrollup from '../components/Scrollup/Scrollup';

class Main extends Component {
    componentDidMount() {
    /*
    TODO: on mount we need to call to the database and retrieve a json with the first 10 sound packs, Hero JSON, Collaboration artist, Merch JSON, FOOTER JSON, Releases
     */
    }

    render() {
        return (
            <div className="main">
                <Header />
                <Hero />
                <SoundPacks />
                <div style={{paddingBottom: 50+'px'}}>
                    <ReleasesComp />
                </div>
                <div style={{paddingBottom: 150+'px'}}>
                    <ColabComp />
                </div>
                <div style={{paddingBottom: 150+'px'}}>
                    <MerchComp />
                </div>
                <Footer />
                <ModalSearch />
                <ModalMenu />
                <Scrollup />
            </div>
        );
    }
}

export default Main;