import React, { Component } from 'react';

import Header from '../components/Header/Header';
// import Breadcrumb from '../components/Breadcrumb/Breadcrumb';
import LoginSection from '../components/Login/Login';
import Footer from '../components/Footer/Footer';
import ModalSearch from '../components/Modal/ModalSearch';
import ModalMenu from '../components/Modal/ModalMenu';
import Scrollup from '../components/Scrollup/Scrollup';

class Login extends Component {
    render() {
        return (
            <div className="main">
                <Header />
                <div style={{padding: '100px'}}/>
                <div style={{color: 'white', display: 'flex', flexDirection: "column", justifyContent:'center', alignItems:'center'}}>
                    <p> You: Trying to go to a page that doesn't exist.</p><br/>
                    <p> Us: </p><br/>
                    <img src="/img/404.gif" alt="404 Error"/>
                </div>
                <Footer/>
                <ModalSearch />
                <ModalMenu />
                <Scrollup />
            </div>
        );
    }
}

export default Login;