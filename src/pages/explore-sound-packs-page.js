import React, { Component } from 'react';

import Header from '../components/Header/Header';
import Breadcrumb from '../components/Breadcrumb/Breadcrumb';
import ExploreSoundPacks from '../components/Explore/ExploreSoundPacks';
import Footer from '../components/Footer/Footer';
import ModalSearch from '../components/Modal/ModalSearch';
import ModalMenu from '../components/Modal/ModalMenu';
import Scrollup from '../components/Scrollup/Scrollup';

class ExploreSoundPacksPage extends Component {
    render() {
        return (
            <div className="main">
                <Header />
                <Breadcrumb title="Sound Packs" bread="breadcrumb-area soundPacks-bg d-flex align-items-center"/>
                <ExploreSoundPacks />
                <Footer />
                <ModalSearch />
                <ModalMenu />
                <Scrollup />
            </div>
        );
    }
}

export default ExploreSoundPacksPage;