import React, { Component } from 'react';

import Header from '../components/Header/Header';
import Breadcrumb from '../components/Breadcrumb/Breadcrumb';
import ItemDetail from '../components/SoundPackDetails/SoundPackDetails';
import LiveAuctions from '../components/Auctions/AuctionsThree';
import Footer from '../components/Footer/Footer';
import ModalSearch from '../components/Modal/ModalSearch';
import ModalMenu from '../components/Modal/ModalMenu';
import Scrollup from '../components/Scrollup/Scrollup';

let jsonData = {
    id: "1",
    img: "/img/auction_1.jpg",
    title: "Walking On Air",
    owner: "Richard",
    price: "$69.00",
    count: "1 of 10",
    packLink: "/sound-pack-details?sp=1",
    group: '["lofi","pop"]',
    itemImg: "/img/auction_2.jpg",
    date: "2022-03-30",
    tab_1: "Tracks",
    producerImg: "/img/avatar_1.jpg",
    producer: "mousepad",
    created: "15 Jul 2021",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",

}


class ItemDetails extends Component {
    state = {
        jsonData: {}
    }

    componentWillMount() {
        this.setState({
            jsonData: jsonData
        })

    }

    render() {
        return (
            <div className="main">
                <Header />
                <ItemDetail jsonData={this.state.jsonData}/>
                <Footer />
                <ModalMenu />
            </div>
        );
    }
}

export default ItemDetails;