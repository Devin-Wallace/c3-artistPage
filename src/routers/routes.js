import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// importing all the pages
import Main from "../pages/main";
import ExploreOne from "../pages/explore-one";
import ExploreSoundPacksPage from "../pages/explore-sound-packs-page";
import ExploreThree from "../pages/explore-three";
import ExploreFour from "../pages/explore-four";
import Auctions from "../pages/auctions";
import SoundPackDetails from "../pages/sound-pack-details-page";
import Activity from "../pages/activity";
import Blog from "../pages/blog";
import BlogSingle from "../pages/blog-single";
import HelpCenter from "../pages/help-center";
import Authors from "../pages/authors";
import Author from "../pages/author";
import Create from "../pages/create";
import Login from "../pages/login";
import Signup from "../pages/signup";
import Contact from "../pages/contact";
import NotFound from "../pages/NotFound";

class MyRouts extends React.Component {
  componentDidMount() {
  //  TODO: get the artist from the URL www.c3.org/mousepad
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/explore-1" component={ExploreOne} />
            <Route exact path="/explore-sound-packs" component={ExploreSoundPacksPage} />
            <Route exact path="/explore-3" component={ExploreThree} />
            <Route exact path="/explore-4" component={ExploreFour} />
            <Route exact path="/auctions" component={Auctions} />
            <Route exact path="/sound-pack-details" component={SoundPackDetails} />
            <Route exact path="/activity" component={Activity} />
            <Route exact path="/blog" component={Blog} />
            <Route exact path="/blog-single" component={BlogSingle} />
            <Route exact path="/help-center" component={HelpCenter} />
            <Route exact path="/authors" component={Authors} />
            <Route exact path="/author" component={Author} />
            <Route exact path="/create" component={Create} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/contact" component={Contact} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}
export default MyRouts;