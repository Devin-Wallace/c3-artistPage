import React, { Component } from 'react';
import axios from 'axios';

const BASE_URL = "https://my-json-server.typicode.com/themeland/netstorm-json/artist";

class ColabComp extends Component {

    data = {
        preHeading: "",
        heading: "Colabs",
        artistData: [
            {
                "id": 1,
                "img": "/img/avatar_1.jpg",
                "artist": "Devin Wallace",
                "handle": "@0xDevbot"
            },
            {
                "id": 2,
                "img": "/img/avatar_2.jpg",
                "artist": "Alan",
                "handle": "@Alan"
            },
            {
                "id": 3,
                "img": "/img/avatar_3.jpg",
                "artist": "Herm",
                "handle": "@Herm"
            },
            {
                "id": 4,
                "img": "/img/avatar_4.jpg",
                "artist": "David",
                "handle": "@WhipTrip36"
            },
            {
                "id": 5,
                "img": "/img/avatar_5.jpg",
                "artist": "Rex",
                "handle": "@Rex"
            },
            {
                "id": 6,
                "img": "/img/avatar_6.jpg",
                "artist": "Andrew",
                "handle": "@ChessBoi"
            }
        ]
    }

    state = {
        data: {},
        artistData: []
    }
    componentDidMount(){
        axios.get(`${BASE_URL}`)
            .then(res => {
                this.setState({
                    data: this.data,
                    artistData: this.data.artistData
                })
                // console.log(this.state.data)
            })
        .catch(err => console.log(err))
    }
    render() {
        return (
            <section className="top-seller-area p-0">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/* Intro */}
                            <div className="intro m-0">
                                <div className="intro-content">
                                    <span>{this.state.data.preHeading}</span>
                                    <h3 className="mt-3 mb-0">Colabs</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row items">
                        {this.data.artistData.map((item, idx) => {
                            return (
                                <div key={`ts_${idx}`} className="col-12 col-sm-6 col-lg-4 item">
                                    {/* Single artist */}
                                    <div className="card no-hover">
                                        <div className="single-seller d-flex align-items-center">
                                            <a href="/author">
                                                <img className="avatar-md rounded-circle" src={item.img} alt="" />
                                            </a>
                                            {/* artist Info */}
                                            <div className="seller-info ml-3">
                                                <a className="seller mb-2" href="/author">{item.artist}</a>
                                                <span>{item.handle}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </section>
        );
    }
}

export default ColabComp;