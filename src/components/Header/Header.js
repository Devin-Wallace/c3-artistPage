import React from 'react';

const Header = () => {
    return (
        <header id="header">
            {/* Navbar */}
            <nav data-aos="zoom-out" data-aos-delay={700} className="navbar navbar-expand">
                <div className="container header">
                    {/* Navbar Brand*/}
                    <a className="navbar-brand" href="/">
                        <img className="navbar-brand-sticky" src={"/img/c3logo.jpg"} alt="sticky brand-logo" />
                    </a>
                    <div className="ml-auto" />
                    {/* Navbar */}
                    <ul className="navbar-nav items mx-4">
                        <li className="nav-item dropdown">
                            <a className="nav-link" href="/">Home</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link" href="#">Explore <i className="fas fa-angle-down ml-1" /></a>
                            <ul className="dropdown-menu">
                                <li className="nav-item"><a href="/explore-sound-packs" className="nav-link">Sound Packs</a></li>
                                <li className="nav-item"><a href="/releases" className="nav-link">Releases</a></li>
                                {/*TODO: Create/Link Releases Page*/}
                                <li className="nav-item"><a href="/Colab" className="nav-link">Colabs</a></li>
                                {/*TODO: Create/Link Releases Page*/}
                                <li className="nav-item"><a href="/merch" className="nav-link">Merch</a></li>
                            {/*    TODO: Create/Link Merch store page*/}

                            </ul>
                        </li>
                        {/*<li className="nav-item">*/}
                        {/*    <a href="/activity" className="nav-link">Activity</a>*/}
                        {/*</li>*/}
                        {/*<li className="nav-item dropdown">*/}
                        {/*    <a className="nav-link" href="#">Community <i className="fas fa-angle-down ml-1" /></a>*/}
                        {/*    <ul className="dropdown-menu">*/}
                        {/*        <li className="nav-item"><a href="/blog" className="nav-link">Blog</a></li>*/}
                        {/*        <li className="nav-item"><a href="/blog-single" className="nav-link">Blog Single</a></li>*/}
                        {/*        <li className="nav-item"><a href="/help-center" className="nav-link">Help Center</a></li>*/}
                        {/*    </ul>*/}
                        {/*</li>*/}
                        {/*<li className="nav-item dropdown">*/}
                        {/*    <a className="nav-link" href="#">Pages <i className="fas fa-angle-down ml-1" /></a>*/}
                        {/*    <ul className="dropdown-menu">*/}
                        {/*        <li className="nav-item"><a href="/authors" className="nav-link">Authors</a></li>*/}
                        {/*        <li className="nav-item"><a href="/author" className="nav-link">Author</a></li>*/}
                        {/*        <li className="nav-item"><a href="/wallet-connect" className="nav-link">Wallet Connect</a></li>*/}
                        {/*        <li className="nav-item"><a href="/create" className="nav-link">Create</a></li>*/}
                        {/*        <li className="nav-item"><a href="/login" className="nav-link">Login</a></li>*/}
                        {/*        <li className="nav-item"><a href="/signup" className="nav-link">Signup</a></li>*/}
                        {/*    </ul>*/}
                        {/*</li>*/}
                        <li className="nav-item">
                            <a href="/contact" className="nav-link">Contact</a>
                        </li>
                    </ul>
                    {/* Navbar Icons */}
                    <ul className="navbar-nav icons">
                        <li className="nav-item">
                            <a href="#" className="nav-link" data-toggle="modal" data-target="#search">
                                <i className="fas fa-search" />
                            </a>
                        </li>
                    </ul>
                    {/* Navbar Toggler */}
                    <ul className="navbar-nav toggle">
                        <li className="nav-item">
                            <a href="#" className="nav-link" data-toggle="modal" data-target="#menu">
                                <i className="fas fa-bars toggle-icon m-0" />
                            </a>
                        </li>
                    </ul>
                    {/* Navbar Action Button */}
                    <ul className="navbar-nav action">
                        {/*<li className="nav-item ml-3">*/}
                        {/*    <a href="/wallet-connect" className="btn ml-lg-auto btn-bordered-white"><i className="icon-wallet mr-md-2" />Wallet Connect</a>*/}
                        {/*</li>*/}
                    </ul>
                </div>
            </nav>
        </header>
    );
};

export default Header;