import React, { Component } from 'react';

const initData = {
    btn_1: "View All",
    btnText: " Play",
    content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    filter_1: "All",
    filter_2: "lofi",
    filter_3: "pop",
    filter_4: "rock",
    filter_5: "free"
}

const data = [
    {
        id: "1",
        img: "/img/auction_1.jpg",
        title: "Walking On Air",
        owner: "Richard",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=1",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "2",
        img: "/img/auction_2.jpg",
        title: "Domain Names",
        owner: "John Deo",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=2",
        group: '["lofi","pop","free"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "3",
        img: "/img/auction_3.jpg",
        title: "Trading Cards",
        owner: "Arham",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=3",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "4",
        img: "/img/auction_4.jpg",
        title: "Industrial Revolution",
        owner: "Yasmin",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=4",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "5",
        img: "/img/auction_5.jpg",
        title: "Utility",
        owner: "Junaid",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=5",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "6",
        img: "/img/auction_6.jpg",
        title: "Sports",
        owner: "ArtNox",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=6",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "7",
        img: "/img/auction_7.jpg",
        title: "Cartoon Heroes",
        owner: "Junaid",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=7",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "8",
        img: "/img/auction_8.jpg",
        title: "Gaming Chair",
        owner: "Johnson",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=8",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "9",
        img: "/img/auction_9.jpg",
        title: "Photography",
        owner: "Sara",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=9",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "10",
        img: "/img/auction_10.jpg",
        title: "Zed Run",
        owner: "SpaceMan",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=10",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "11",
        img: "/img/auction_11.jpg",
        title: "Rare Tyres",
        owner: "Monas",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=11",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "12",
        img: "/img/auction_12.jpg",
        title: "World of Women",
        owner: "Victor",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=12",
        group: '["lofi"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    }
]

class ExploreSoundPacks extends Component {
    state = {
        initData: {},
        data: []
    }
    componentDidMount(){
        this.setState({
            initData: initData,
            data: data
        })
    }
    render() {
        return (
            <section className="explore-area">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 col-md-8 col-lg-7">
                            {/* Intro */}
                            <div className="intro text-center mb-4">
                                {/*<span>{this.state.initData.pre_heading}</span>*/}
                                {/*<h3 className="mt-3 mb-0">{this.state.initData.heading}</h3>*/}
                                <p>{this.state.initData.content}</p>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center text-center">
                        <div className="col-12">
                            {/* Explore Menu */}
                            <div className="explore-menu btn-group btn-group-toggle flex-wrap justify-content-center text-center mb-4" data-toggle="buttons">
                                <label className="btn active d-table text-uppercase p-2">
                                    <input type="radio" defaultValue="all" defaultChecked className="explore-btn" />
                                    <span>{this.state.initData.filter_1}</span>
                                </label>
                                <label className="btn d-table text-uppercase p-2">
                                    <input type="radio" defaultValue="lofi" className="explore-btn" />
                                    <span>{this.state.initData.filter_2}</span>
                                </label>
                                <label className="btn d-table text-uppercase p-2">
                                    <input type="radio" defaultValue="pop" className="explore-btn" />
                                    <span>{this.state.initData.filter_3}</span>
                                </label>
                                <label className="btn d-table text-uppercase p-2">
                                    <input type="radio" defaultValue="rock" className="explore-btn" />
                                    <span>{this.state.initData.filter_4}</span>
                                </label>
                                <label className="btn d-table text-uppercase p-2">
                                    <input type="radio" defaultValue="free" className="explore-btn" />
                                    <span>{this.state.initData.filter_5}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="row items explore-items">
                        {this.state.data.map((item, idx) => {
                            return (
                                <div key={`edth_${idx}`} className="col-12 col-sm-6 col-lg-3 item explore-item" data-groups={item.group}>
                                    <div className="card">
                                        <div className="image-over">
                                            <a href="/sound-pack-details">
                                                <img className="card-img-top" src={item.img} alt="" />
                                            </a>
                                        </div>
                                        {/* Card Caption */}
                                        <div className="card-caption col-12 p-0">
                                            {/* Card Body */}
                                            <div className="card-body">
                                                <a href="/sound-pack-details">
                                                    <h5 className="mb-0">{item.title}</h5>
                                                </a>
                                                <div className="seller d-flex align-items-center my-3">
                                                    <span>Produced By</span>
                                                    <h6 className="ml-2 mb-0">{item.producer}</h6>
                                                </div>
                                                <div className="card-bottom d-flex justify-content-between">
                                                    <span>{item.price}</span>
                                                    <span>{item.count}</span>
                                                </div>
                                                <a className="btn btn-bordered-white btn-smaller mt-3" href={item.packLink}><i className="fas fa-play"/> {this.state.initData.btnText}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </section>
        );
    }
}

export default ExploreSoundPacks;