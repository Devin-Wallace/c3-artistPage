import React, { Component } from 'react';

const initData = {
    pre_heading: "",
    heading: "Merch",
    btn_1: "View All",
    btn_2: "Load More",
    btnText: "Add To Cart"

}

const data = [
    {
        id: "1",
        img: "/img/auction_1.jpg",
        title: "Walking On Air",
        price: "$69.00",
    },
    {
        id: "2",
        img: "/img/auction_2.jpg",
        title: "Domain Names",
        price: "$69.00",
    },
    {
        id: "3",
        img: "/img/auction_3.jpg",
        title: "Trading Cards",
        price: "$69.00",
    },
    {
        id: "4",
        img: "/img/auction_4.jpg",
        title: "Industrial Revolution",
        price: "$69.00",
    },
    {
        id: "5",
        img: "/img/auction_5.jpg",
        title: "Utility",
        price: "$69.00",
    },
    {
        id: "6",
        img: "/img/auction_6.jpg",
        title: "Sports",
        price: "$69.00",
    },
    {
        id: "7",
        img: "/img/auction_7.jpg",
        title: "Cartoon Heroes",
        price: "$69.00",
    },
    {
        id: "8",
        img: "/img/auction_8.jpg",
        title: "Gaming Chair",
        price: "$69.00",
    },
    {
        id: "9",
        img: "/img/auction_9.jpg",
        title: "Photography",
        price: "$69.00",
    },
    {
        id: "10",
        img: "/img/auction_10.jpg",
        title: "Zed Run",
        price: "$69.00",
    },
    {
        id: "11",
        img: "/img/auction_11.jpg",
        title: "Rare Tyres",
        price: "$69.00",
    },
    {
        id: "12",
        img: "/img/auction_12.jpg",
        title: "World of Women",
        price: "$69.00",
    }
]

class MerchComp extends Component {
    state = {
        initData: {},
        data: []
    }
    componentDidMount(){
        this.setState({
            initData: initData,
            data: data
        })
    }
    render() {
        return (
            <section className="explore-area load-more p-0">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/* Intro */}
                            <div className="intro d-flex justify-content-between align-items-end m-0">
                                <div className="intro-content">
                                    <span>{this.state.initData.pre_heading}</span>
                                    <h3 className="mt-3 mb-0">{this.state.initData.heading}</h3>
                                </div>
                                <div className="intro-btn">
                                    <a className="btn content-btn" href="/c3-Marketplace">{this.state.initData.btn_1}</a>
                                {/*    TODO: convert explore-3 to merch store*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row items">
                        {this.state.data.map((item, idx) => {
                            return (
                                <div key={`exo_${idx}`} className="col-12 col-sm-6 col-lg-3 item">
                                    <div className="card">
                                        <div className="image-over">
                                            <a href="/item-details">
                                                {/*TODO: create merch items-details comp, this one is for the overall card/ image */}
                                                <img className="card-img-top" src={item.img} alt="" />
                                            </a>
                                        </div>
                                        {/* Card Caption */}
                                        <div className="card-caption col-12 p-0">
                                            {/* Card Body */}
                                            <div className="card-body">
                                                <a href="/bitchs">
                                                    {/*TODO: set to item merch details, this one is for the name of the product*/}
                                                    <h5 className="mb-0">{item.title}</h5>
                                                </a>
                                                <div className="card-bottom d-flex justify-content-between">
                                                    <span>{item.price}</span>
                                                </div>
                                                <a className="btn btn-bordered-white btn-smaller mt-3" href="/login"><i className="fas fa-shopping-cart"/> {initData.btnText}</a>
                                            {/*    TODO: set /login to merch store*/}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                    <div className="row">
                        <div className="col-12 text-center">
                            <a id="load-btn" className="btn btn-bordered-white mt-5" href="#">{this.state.initData.btn_2}</a>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default MerchComp;