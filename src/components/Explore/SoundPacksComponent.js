import React, { Component } from 'react';

const initData = {
    pre_heading: "",
    heading: "Sound Packs",
    btn_1: "View All",
    btnText: " Play"
}

const data = [
    {
        id: "1",
        img: "/img/auction_1.jpg",
        title: "Walking On Air",
        owner: "Richard",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=1",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "2",
        img: "/img/auction_2.jpg",
        title: "Domain Names",
        owner: "John Deo",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=2",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "3",
        img: "/img/auction_3.jpg",
        title: "Trading Cards",
        owner: "Arham",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=3",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "4",
        img: "/img/auction_4.jpg",
        title: "Industrial Revolution",
        owner: "Yasmin",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=4",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "5",
        img: "/img/auction_5.jpg",
        title: "Utility",
        owner: "Junaid",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=5",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "6",
        img: "/img/auction_6.jpg",
        title: "Sports",
        owner: "ArtNox",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=6",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "7",
        img: "/img/auction_7.jpg",
        title: "Cartoon Heroes",
        owner: "Junaid",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=7",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "8",
        img: "/img/auction_8.jpg",
        title: "Gaming Chair",
        owner: "Johnson",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=8",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "9",
        img: "/img/auction_9.jpg",
        title: "Photography",
        owner: "Sara",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=9",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "10",
        img: "/img/auction_10.jpg",
        title: "Zed Run",
        owner: "SpaceMan",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=10",
        group: '["lofi","pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "11",
        img: "/img/auction_11.jpg",
        title: "Rare Tyres",
        owner: "Monas",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=11",
        group: '["pop"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    },
    {
        id: "12",
        img: "/img/auction_12.jpg",
        title: "World of Women",
        owner: "Victor",
        price: "$69.00",
        count: "1 of 1",
        btnText: "Place a Bid",
        packLink: "/sound-pack-details?sp=12",
        group: '["lofi"]',
        itemImg: "/img/auction_2.jpg",
        date: "2022-03-30",
        tab_1: "Tracks",
        ownerImg: "/img/avatar_1.jpg",
        producer: "mousepad",
        created: "15 Jul 2021",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.",
    }
]

class SoundPacksCompenent extends Component {
    state = {
        initData: {},
        data: []
    }
    componentDidMount(){
        this.setState({
            initData: initData,
            data: data
        })
    }
    render() {
        return (
            <section className="live-auctions-area" >
                <div className="container" style={{maxHeight: 460+'px'}}>
                    <div className="row">
                        <div className="col-11">
                            {/* Intro */}
                            <div className="intro d-flex justify-content-between align-items-end m-0">
                                <div className="intro-content">
                                    <span>{this.state.initData.pre_heading}</span>
                                    <h3 className="mt-3 mb-0">{this.state.initData.heading}</h3>
                                </div>
                                <div className="intro-btn">
                                    <a className="btn content-btn" href="/explore-sound-packs">{this.state.initData.btn_1}</a>
                                {/*TODO:: on this href we will need to call to the database and retrieve the soundpacks json file*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="auctions-slides">
                        <div className="swiper-container slider-mid items">
                            <div className="swiper-wrapper">
                                {/* Single Slide */}
                                {this.state.data.map((item, idx) => {
                                    return (
                                        <div key={`auc_${idx}`} className="swiper-slide item">
                                            <div className="card">
                                                <div className="image-over">
                                                    <a href={item.packLink}>
                                                        <img className="card-img-top" src={item.img} alt="" />
                                                    </a>
                                                </div>
                                                {/* Card Caption */}
                                                <div className="card-caption col-12 p-0">
                                                    {/* Card Body */}
                                                    <div className="card-body">
                                                        <a href={item.packLink}>
                                                            <h5 className="mb-0">{item.title}</h5>
                                                        </a>
                                                        <div className="card-bottom d-flex justify-content-between">
                                                            <span>{item.price}</span>
                                                        </div>
                                                        <a className="btn btn-bordered-white btn-smaller mt-3" href={item.packLink}><i className="fas fa-play"/> {this.state.initData.btnText}</a>
                                                    {/*TODO: on this href we will need to open soundpacks data with the data already given basd on ID*/}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="swiper-pagination" />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default SoundPacksCompenent;