import React, { Component } from 'react';
import axios from 'axios';

const BASE_URL = "https://my-json-server.typicode.com/themeland/netstorm-json/collections";

class ReleasesComp extends Component {
    data =  {
        "preHeading": "",
        "heading": "Releases",
        "btnText": "View All",
        collectionData: [
            {
                "id": 1,
                "img": "/img/author_1.jpg",
                "avatar": "/img/avatar_1.jpg",
                "title": "Worlds",
                "content": "ERC-729"
            },
            {
                "id": 2,
                "img": "/img/author_2.jpg",
                "avatar": "/img/avatar_2.jpg",
                "title": "Digital Arts",
                "content": "ERC-522"
            },
            {
                "id": 3,
                "img": "/img/author_3.jpg",
                "avatar": "/img/avatar_3.jpg",
                "title": "Sports",
                "content": "ERC-495"
            },
            {
                "id": 4,
                "img": "/img/author_4.jpg",
                "avatar": "/img/avatar_4.jpg",
                "title": "Photography",
                "content": "ERC-873"
            },
            {
                "id": 5,
                "img": "/img/author_5.jpg",
                "avatar": "/img/avatar_5.jpg",
                "title": "Trading Cards",
                "content": "ERC-397"
            },
            {
                "id": 6,
                "img": "/img/author_6.jpg",
                "avatar": "/img/avatar_6.jpg",
                "title": "Walking On Air",
                "content": "ERC-403"
            },
            {
                "id": 7,
                "img": "/img/author_7.jpg",
                "avatar": "/img/avatar_7.jpg",
                "title": "Domain Names",
                "content": "ERC-687"
            },
            {
                "id": 8,
                "img": "/img/author_8.jpg",
                "avatar": "/img/avatar_8.jpg",
                "title": "Collectibles",
                "content": "ERC-972"
            }
        ]
    }

    state = {
        data: {},
        collectionData: []
    }
    componentDidMount(){
        axios.get(`${BASE_URL}`)
            .then(res => {
                this.setState({
                    data: this.data,
                    collectionData: this.data.collectionData
                })
                // console.log(this.state.data)
            })
        .catch(err => console.log(err))
    }
    render() {
        return (
            <section className="popular-collections-area">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            {/* Intro */}
                            <div className="intro d-flex justify-content-between align-items-end m-0">
                                <div className="intro-content">
                                    <span>{this.state.data.preHeading}</span>
                                    <h3 className="mt-3 mb-0">{this.state.data.heading}</h3>
                                </div>
                                <div className="intro-btn">
                                    <a className="btn content-btn text-left" href="/explore-2">{this.state.data.btnText}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row items">
                        {this.state.collectionData.map((item, idx) => {
                            return (
                                <div key={`cd_${idx}`} className="col-12 col-sm-6 col-lg-3 item">
                                    <div className="card no-hover text-center">
                                        <div className="image-over">
                                            <a href="/item-details">
                                                <img className="card-img-top" src={item.img} alt="" />
                                            </a>
                                            {/* Seller */}
                                            <a className="seller" href="/item-details">
                                                <div className="seller-thumb avatar-lg">
                                                    <img className="rounded-circle" src={item.avatar} alt="" />
                                                </div>
                                            </a>
                                        </div>
                                        {/* Card Caption */}
                                        <div className="card-caption col-12 p-0">
                                            {/* Card Body */}
                                            <div className="card-body mt-4">
                                                <a href="/item-details">
                                                    <h5 className="mb-2">{item.title}</h5>
                                                </a>
                                                <span>{item.content}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </section>
        );
    }
}

export default ReleasesComp;