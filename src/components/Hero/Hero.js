import React, { Component } from 'react';


const initData = {
    heading: "Mousepad Music",
    content: "Sound packs and other shit",
    btn_1: "Spotify",
    btn_2: "Soundcloud"
}

class Hero extends Component {
    state = {
        data: {}
    }
    componentDidMount(){
        this.setState({
            data: initData
        })
    }
    render() {
        return (
            <div className="backgroundHero">
                <section className="hero-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-7">
                                <h1 className="mt-4">{this.state.data.heading}</h1>
                                <p>{this.state.data.content}</p>
                                {/* Buttons */}
                                <div className="button-group">
                                    <a className="btn btn-bordered-white" href="https://open.spotify.com/artist/4S5BKypzX8ejWQdb4Wj0YP?si=hhzOe_wySdCo76IMBxRUYQ" target="_blank"><i className="fab fa-spotify"/> {this.state.data.btn_1}</a>
                                    <a className="btn btn-bordered-white" href="https://soundcloud.com/josh-harris-487269049" target="_blank"><i className="fab fa-soundcloud"/> {this.state.data.btn_2}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Hero;